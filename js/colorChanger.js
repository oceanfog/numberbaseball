var gv_sc_color = 0;


var ar_DOMObjectName = [
	'header',
	'nav',
	'aside',
	'section',
	'footer',
	'body',
	'article'
];

var gv_ar_colorSet = [
	["#CFDDE8"], ["#D2D8DD"], ["#FFE291"], ["#cfdde8"],
	["#5478C8"], ["#EEF4FB"], ["#E6EEF0"]
];

var gv_ar_colorSet2 = [
	["#11282D"], ["#DEC9AC"], ["#CCA18B"], ["#A5C4BB"],
	["#11282D"], ["#776D53"], ["#FAF1D5"]
];



function initBody( bodyId ){
	//'body#bodyId'
	if(bodyId = ""){
		jQuery('body#bodyId').empty();
	}else{
		jQuery(bodyId).empty();
	}
}

function setColorEachObject( ar_DOMObjectName, ar_colorSet ){
	jQuery(ar_DOMObjectName).each(function(key, val){
		//console.log(key + ":" + val);
		jQuery( ar_DOMObjectName[key] ).css({ "background-color": ar_colorSet[key] });
	});
}

function setColorStatus(){
	if ( gv_sc_color == 1){
		gv_sc_color = 0;
	}else if( gv_sc_color == 0){
		gv_sc_color = 1;
	}else {
		gv_sc_color = 0;
	}

	return gv_sc_color;	
}
